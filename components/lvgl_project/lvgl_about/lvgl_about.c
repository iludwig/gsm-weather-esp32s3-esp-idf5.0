#include "lvgl_about.h"
#include "lvgl_Project.h"
//#include "lvgl_SheZhi.h"
#include "lvgl_about_anim.h"

#if 1
#define about_debug(format, ...) lvgl_project_debug("[关于]- ",format,##__VA_ARGS__);
#else
#define about_debug(format, ...) ;
#endif

LV_IMG_DECLARE(bmp_guanyu)

_lvgl_about_function_data lvgl_about_function_data;
_lvgl_about_wm lvgl_about_wm;

lv_obj_t *lvgl_about_main_cont = NULL;
lv_style_t lvgl_about_style;

void lvgl_about_event_cb(struct _lv_obj_t * obj, lv_event_t event)
{
	if (obj == lvgl_about_main_cont)
	{
		//about_debug("cb:%d\r\n", event);
		switch (event)
		{

		case LV_EVENT_DELETE:

			lvgl_about_closed();
			break;

		case LV_EVENT_PRESSING://对象被持续按下


			break;
		case LV_EVENT_PRESSED://对象已被按下


			break;
		case LV_EVENT_DRAG_END://拖动结束后


			if (lv_obj_get_y(lvgl_about_main_cont) != 0)
			{

				lv_obj_set_y(lvgl_about_main_cont, 0);
			}

			if (lv_obj_get_x(lvgl_about_main_cont) != 0)
			{
				if (lv_obj_get_x(lvgl_about_main_cont) > 100)
				{


					lvgl_about_close();


				}
				else
				{
					lv_obj_set_x(lvgl_about_main_cont, 0);
				}
			}
			break;
			break;

		case LV_EVENT_RELEASED://按钮释放

		case LV_EVENT_LONG_PRESSED://按钮长按

			break;

		default:
			break;

		}

	}
}


void lvgl_about_create(lv_obj_t * Fu)
{

	if (lvgl_about_main_cont == NULL)
	{

		lv_style_init(&lvgl_about_style);
		lv_style_copy(&lvgl_about_style, &lvgl_WuBianKuang_cont_style);
		/*渐变色*/
		lv_style_set_bg_opa(&lvgl_about_style, LV_STATE_DEFAULT, LV_OPA_COVER);//背景透明度
		//lv_style_set_bg_color(&lvgl_about_style, LV_STATE_DEFAULT, lv_color_hex(0x1b1d5c));//背景上面颜色
		//lv_style_set_bg_grad_color(&lvgl_about_style, LV_STATE_DEFAULT, lv_color_hex(0x5c418d));//背景上面颜色
		lv_style_set_bg_color(&lvgl_about_style, LV_STATE_DEFAULT, LV_COLOR_MAKE(0x29,0xab,0xe2));//背景上面颜色
		lv_style_set_bg_grad_color(&lvgl_about_style, LV_STATE_DEFAULT,  LV_COLOR_MAKE(0x29,0xab,0xe2));//背景上面颜色
		lv_style_set_bg_grad_dir(&lvgl_about_style, LV_STATE_DEFAULT, LV_GRAD_DIR_VER);//渐变方向

		/*调整渐变色位置*/
		lv_style_set_bg_main_stop(&lvgl_about_style, LV_STATE_DEFAULT, 10);
		lv_style_set_bg_main_stop(&lvgl_about_style, LV_STATE_DEFAULT, 100);

		//----创建容器----//
		lvgl_about_main_cont = lv_cont_create(Fu, NULL);
		lv_obj_set_pos(lvgl_about_main_cont, 0, 0);
		lv_obj_set_size(lvgl_about_main_cont, lv_obj_get_width(Fu), lv_obj_get_height(Fu));
		//lv_obj_set_drag_parent(lvgl_about_main_cont, true); //启用 / 禁用父对象可拖动

		lv_obj_set_click(lvgl_about_main_cont, true); //启用 / 禁用可点击
		lv_obj_set_drag(lvgl_about_main_cont, true);//启用/禁用对象可拖动
		lv_obj_set_drag_dir(lvgl_about_main_cont, LV_DRAG_DIR_ONE);//设置拖动方向
		lv_obj_set_drag_throw(lvgl_about_main_cont, false);//启用/禁用拖动后是否有惯性移动

		//lv_obj_set_drag_parent(lvgl_about_main_cont, true); //启用 / 禁用父对象可拖动
		lv_obj_add_style(lvgl_about_main_cont, LV_OBJ_PART_MAIN, &lvgl_about_style);//设置样式
		lv_obj_set_event_cb(lvgl_about_main_cont, lvgl_about_event_cb);//设置回调函数


        lvgl_about_wm.label1 = lv_label_create(lvgl_about_main_cont, NULL);
        lv_obj_set_pos(lvgl_about_wm.label1, 0, 0);
        lv_label_set_long_mode(lvgl_about_wm.label1, LV_LABEL_LONG_EXPAND);
        lv_obj_set_size(lvgl_about_wm.label1, lv_obj_get_width(lvgl_about_main_cont),20);
        lv_label_set_recolor(lvgl_about_wm.label1, true);
        lv_label_set_align(lvgl_about_wm.label1, LV_LABEL_ALIGN_CENTER);
        lv_label_set_recolor(lvgl_about_wm.label1, true);
        lv_label_set_text(lvgl_about_wm.label1, lvgl_globa_text[78][system_get_Language()]);
        // lv_obj_add_style(lvgl_about_wm.label1, LV_OBJ_PART_MAIN, &lvgl_style.font_ChangYongHanZi_24);
		lv_obj_set_style_local_text_font(lvgl_about_wm.label1, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT, (const lv_font_t*)&font_ChangYongHanZi_16);
		lv_obj_set_style_local_text_color(lvgl_about_wm.label1, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_WHITE);
		lv_obj_align(lvgl_about_wm.label1, NULL, LV_ALIGN_IN_TOP_MID, 0, 5);

        lvgl_about_wm.label2 = lv_label_create(lvgl_about_main_cont,lvgl_about_wm.label1);

        lv_label_set_long_mode(lvgl_about_wm.label2, LV_LABEL_LONG_SROLL);
        lv_label_set_align(lvgl_about_wm.label2, LV_LABEL_ALIGN_LEFT);
        lv_label_set_text(lvgl_about_wm.label2, lvgl_globa_text[79][system_get_Language()]);
        // lv_obj_add_style(lvgl_about_wm.label2, LV_OBJ_PART_MAIN, &lvgl_style.font_changyong_16_white_bg);
        lv_label_set_anim_speed(lvgl_about_wm.label2, 10);

        sprintf(lvgl_about_wm.p_buf,"%s%s\n%s%s\n%s%s\n%s%s\n%s%s\n%s%s\n\n",
            lvgl_globa_text[69][system_get_Language()],
            system_data.productname,
            lvgl_globa_text[79][system_get_Language()],
            system_data.wifi_name,
            lvgl_globa_text[80][system_get_Language()],
            system_data.wifi_password,
            lvgl_globa_text[83][system_get_Language()],
            system_data.public_IP_ADDR,
            lvgl_globa_text[81][system_get_Language()],
            system_data.IP_ADDR,
            lvgl_globa_text[82][system_get_Language()],
            system_data.bilibili_Data.id
        );
        lv_obj_set_size(lvgl_about_wm.label2,
        lv_obj_get_width(lvgl_about_main_cont),
        lv_obj_get_height(lvgl_about_main_cont)-(lv_obj_get_y(lvgl_about_wm.label1)+lv_obj_get_height(lvgl_about_wm.label1))
        );
        lv_label_set_text(lvgl_about_wm.label2,lvgl_about_wm.p_buf);
        lv_obj_align(lvgl_about_wm.label2, NULL, LV_ALIGN_IN_TOP_LEFT, 10, lv_obj_get_y(lvgl_about_wm.label1)+lv_obj_get_height(lvgl_about_wm.label1));

		lvgl_about_wm.image1 = lv_img_create(lvgl_about_main_cont, NULL);
		lv_obj_set_pos(lvgl_about_wm.image1, 0, 0);
		lv_img_set_src(lvgl_about_wm.image1, &bmp_guanyu);
        lv_obj_align(lvgl_about_wm.image1,NULL, LV_ALIGN_IN_BOTTOM_MID, 0, -20);

        lvgl_about_wm.label3 = lv_label_create(lvgl_about_main_cont,lvgl_about_wm.label1);

        lv_label_set_long_mode(lvgl_about_wm.label3, LV_LABEL_LONG_EXPAND);
        lv_label_set_align(lvgl_about_wm.label3, LV_LABEL_ALIGN_CENTER);
        lv_label_set_anim_speed(lvgl_about_wm.label3, 10);
        lv_obj_set_size(lvgl_about_wm.label3,lv_obj_get_width(lvgl_about_main_cont),20);
        lv_label_set_text(lvgl_about_wm.label3,"野生程序猿");
		lv_obj_align(lvgl_about_wm.label3,NULL, LV_ALIGN_IN_BOTTOM_MID, 0, -2);

		lvgl_task_create(&lvgl_about_wm.lvgl_task,lvgl_about_TaskCb, 100, LV_TASK_PRIO_HIGH, NULL);

		about_debug("创建窗口\r\n");
	}
	else
	{

		about_debug("显示窗口\r\n");
	}
	lv_obj_move_background(lvgl_about_main_cont);

	lv_obj_set_drag_throw(lvgl_about_main_cont, false);//启用/禁用拖动后是否有惯性移动
	lv_obj_set_pos(lvgl_about_main_cont, 0, 0);
	lvgl_set_obj_show(lvgl_about_main_cont);

	//lvgl_about_anim_Jin();

}


void lvgl_about_TaskCb(lv_task_t *t)
{
	static int time=0;

	if (lv_obj_get_y(lvgl_about_main_cont) != 0)
	{
		lv_obj_set_drag_throw(lvgl_about_main_cont, true);//启用/禁用拖动后是否有惯性移动

	}

	if (lv_obj_get_x(lvgl_about_main_cont) != 0)
	{
		lv_obj_set_drag_throw(lvgl_about_main_cont, false);//启用/禁用拖动后是否有惯性移动
	}

}



void lvgl_about_close(void)
{


	lvgl_desktop_create(lv_scr_act());

	lvgl_task_delete(&lvgl_about_wm.lvgl_task);

	lvgl_about_function_data.point.y = lv_obj_get_y(lvgl_about_main_cont);
	lvgl_about_function_data.point.x = lv_obj_get_x(lvgl_about_main_cont);

	lv_obj_clean(lvgl_about_main_cont);

	//uint16_t count = lv_obj_count_children(lvgl_about_main_cont);
	//printf("count:%d\r\n", count);

	lv_obj_del(lvgl_about_main_cont);
}


void lvgl_about_closed(void)
{
	about_debug("删除窗口\r\n");
	about_debug("closed\r\n");

	lvgl_about_main_cont = NULL;



}
