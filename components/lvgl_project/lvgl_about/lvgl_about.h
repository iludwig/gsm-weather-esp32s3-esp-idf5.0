#ifndef __LVGL_about_H__
#define __LVGL_about_H__

#include "lvgl.h"
#include "lvgl_Project.h"
#include "my_system.h"



#define lvgl_about_x 0
#define lvgl_about_y 0
#define lvgl_about_xsize 240
#define lvgl_about_ysize 240

typedef struct
{
	char p_buf[300];
	lv_obj_t * calendar;

	lv_task_t * lvgl_task;

	lv_style_t *btn_XuanZhong_style;
	lv_obj_t *btn_XuanZhong;

	lv_anim_t lv_anim_jin;
	lv_anim_t lv_anim_chu;

    lv_obj_t* label1;
	lv_obj_t* label2;
	lv_obj_t* label3;
	lv_obj_t* label4;
	lv_obj_t* image1;


}_lvgl_about_wm;

extern _lvgl_about_wm lvgl_about_wm;
typedef struct
{
	char buf[20];


	int press_num;//按下的按钮编号
	lv_obj_t *AnXia_button;//按下的按钮句柄

	int DongHua_Flag;

	lv_point_t point;


}_lvgl_about_function_data;

extern _lvgl_about_function_data lvgl_about_function_data;

extern lv_obj_t *lvgl_about_main_cont;

void lvgl_about_create(lv_obj_t * Fu);
void lvgl_about_close(void);
void lvgl_about_closed(void);
void lvgl_about_TaskCb(lv_task_t *t);


#endif
